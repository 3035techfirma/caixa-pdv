function isWeb() {
  return !navigator.userAgent.includes("Electron");
}

function isElectron() {
  return navigator.userAgent.includes("Electron");
}

export { isWeb, isElectron };
