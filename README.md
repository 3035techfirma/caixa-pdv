README
This project was bootstrapped with Randy Findley's [Create React App | Electron](https://github.com/rgfindl/electron-cra-boilerplate) boilerplate.

You can read Randy Findley's article [here](https://www.codementor.io/randyfindley/how-to-build-an-electron-app-using-create-react-app-and-electron-builder-ss1k0sfer) for more info.

It's preferable to use yarn package manager. [Click here for more info](https://yarnpkg.com/)

## Available Scripts

In the project directory, you can run:

### yarn install

After cloning the repository run `yarn install` to download all the dependencies.

### yarn electron-dev

Runs the Electron and React app at the same time in development mode.

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.
You will also see any lint errors in the console.

### yarn electron-pack

Builds the app for production to the `build` and `dist` folders.

It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.

Your app is ready to be deployed!

More information on:

- [react deployment](https://facebook.github.io/create-react-app/docs/deployment)
- [electron-builder](https://www.electron.build/)
